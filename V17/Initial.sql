-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema db_test
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `db_test` ;

-- -----------------------------------------------------
-- Schema db_test
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `db_test` ;
USE `db_test` ;

-- -----------------------------------------------------
-- Table `db_test`.`articlecategories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_test`.`articlecategories` ;

CREATE TABLE IF NOT EXISTS `db_test`.`articlecategories` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Category` VARCHAR(45) NOT NULL,
  `Accessory` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
;


-- -----------------------------------------------------
-- Table `db_test`.`articles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_test`.`articles` ;

CREATE TABLE IF NOT EXISTS `db_test`.`articles` (
  `ArticleNo` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Description` VARCHAR(999) NULL DEFAULT NULL,
  `Price` DECIMAL(12,2) NOT NULL,
  `Category` INT(11) NOT NULL,
  `PictureLink` VARCHAR(45) NOT NULL,
  `MinStock` INT(11) NOT NULL,
  `CurrentStock` INT(11) NOT NULL,
  `Status` VARCHAR(45) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`ArticleNo`),
  CONSTRAINT `FK_Art_Cat`
    FOREIGN KEY (`Category`)
    REFERENCES `db_test`.`articlecategories` (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 1002
;

CREATE INDEX `FK_Art_Cat_idx` ON `db_test`.`articles` (`Category` ASC) VISIBLE;

CREATE FULLTEXT INDEX `Fulltext_Article` ON `db_test`.`articles` (`Name`, `Description`) VISIBLE;


-- -----------------------------------------------------
-- Table `db_test`.`my_stopwords`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_test`.`my_stopwords` ;

CREATE TABLE IF NOT EXISTS `db_test`.`my_stopwords` (
  `value` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`value`))
ENGINE = InnoDB
;


-- -----------------------------------------------------
-- Table `db_test`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_test`.`users` ;

CREATE TABLE IF NOT EXISTS `db_test`.`users` (
  `CustomerNo` INT(11) NOT NULL AUTO_INCREMENT,
  `EMail` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(45) NULL DEFAULT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `Firstname` VARCHAR(45) NOT NULL,
  `Title` VARCHAR(45) NULL DEFAULT NULL,
  `FormOfAddress` VARCHAR(45) NOT NULL,
  `InvoiceStreet` VARCHAR(45) NOT NULL,
  `InvoiceHouseNumber` VARCHAR(45) NOT NULL,
  `InvoicePostalCode` VARCHAR(45) NOT NULL,
  `InvoiceTown` VARCHAR(45) NOT NULL,
  `OneTimeCustomer` VARCHAR(45) NOT NULL,
  `Admin` VARCHAR(45) NOT NULL DEFAULT 'false',
  PRIMARY KEY (`CustomerNo`))
ENGINE = InnoDB
AUTO_INCREMENT = 39
;

CREATE UNIQUE INDEX `EMail_UNIQUE` ON `db_test`.`users` (`EMail` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `db_test`.`orderstatuses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_test`.`orderstatuses` ;

CREATE TABLE IF NOT EXISTS `db_test`.`orderstatuses` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Statustext` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
;


-- -----------------------------------------------------
-- Table `db_test`.`orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_test`.`orders` ;

CREATE TABLE IF NOT EXISTS `db_test`.`orders` (
  `OrderNo` INT(11) NOT NULL AUTO_INCREMENT,
  `CustomerNo` INT(11) NOT NULL,
  `Status` INT(11) NOT NULL,
  `DeliveryStreet` VARCHAR(45) NOT NULL,
  `DeliveryHouseNumber` VARCHAR(45) NOT NULL,
  `DeliveryPostalCode` VARCHAR(45) NOT NULL,
  `DeliveryTown` VARCHAR(45) NOT NULL,
  `InvoiceStreet` VARCHAR(45) NOT NULL,
  `InvoiceHouseNumber` VARCHAR(45) NOT NULL,
  `InvoicePostalCode` VARCHAR(45) NOT NULL,
  `InvoiceTown` VARCHAR(45) NOT NULL,
  `CreationDate` DATE NOT NULL,
  `PaymentMethod` VARCHAR(45) NOT NULL,
  `DispatchType` VARCHAR(45) NOT NULL,
  `DeliveryName` VARCHAR(45) NOT NULL,
  `DeliveryFirstname` VARCHAR(45) NOT NULL,
  `InvoiceName` VARCHAR(45) NOT NULL,
  `InvoiceFirstname` VARCHAR(45) NOT NULL,
  `DeliveryDate` DATE NOT NULL,
  PRIMARY KEY (`OrderNo`),
  CONSTRAINT `FK_Ord_Cust`
    FOREIGN KEY (`CustomerNo`)
    REFERENCES `db_test`.`users` (`CustomerNo`),
  CONSTRAINT `FK_Ord_Stat`
    FOREIGN KEY (`Status`)
    REFERENCES `db_test`.`orderstatuses` (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 16
;

CREATE INDEX `FK_Ord_Cust_idx` ON `db_test`.`orders` (`CustomerNo` ASC) VISIBLE;

CREATE INDEX `FK_Ord_Stat_idx` ON `db_test`.`orders` (`Status` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `db_test`.`orderpositions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_test`.`orderpositions` ;

CREATE TABLE IF NOT EXISTS `db_test`.`orderpositions` (
  `OrderNo` INT(11) NOT NULL,
  `ArticleNo` INT(11) NOT NULL,
  `Amount` INT(11) NOT NULL,
  `PricePerPiece` DECIMAL(12,2) NOT NULL,
  PRIMARY KEY (`OrderNo`, `ArticleNo`),
  CONSTRAINT `FK_OrdPos_Art`
    FOREIGN KEY (`ArticleNo`)
    REFERENCES `db_test`.`articles` (`ArticleNo`),
  CONSTRAINT `FK_OrdPos_Ord`
    FOREIGN KEY (`OrderNo`)
    REFERENCES `db_test`.`orders` (`OrderNo`))
ENGINE = InnoDB
;

CREATE INDEX `FK_OrdPos_Art_idx` ON `db_test`.`orderpositions` (`ArticleNo` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `db_test`.`questionsandanswers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_test`.`questionsandanswers` ;

CREATE TABLE IF NOT EXISTS `db_test`.`questionsandanswers` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Questioner` VARCHAR(45) NOT NULL,
  `Question` VARCHAR(400) NOT NULL,
  `Answer` VARCHAR(400) NULL DEFAULT NULL,
  `Title` VARCHAR(45) NOT NULL,
  `Date` DATE NOT NULL,
  `Active` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
;


-- -----------------------------------------------------
-- Table `db_test`.`reviews`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_test`.`reviews` ;

CREATE TABLE IF NOT EXISTS `db_test`.`reviews` (
  `CustomerNo` INT(11) NOT NULL,
  `ArticleNo` INT(11) NOT NULL,
  `NumberOfStars` INT(11) NOT NULL,
  `Text` VARCHAR(1000) NULL DEFAULT NULL,
  `Title` VARCHAR(45) NOT NULL,
  `Date` DATE NOT NULL,
  PRIMARY KEY (`CustomerNo`, `ArticleNo`),
  CONSTRAINT `FK_Rev_Art`
    FOREIGN KEY (`ArticleNo`)
    REFERENCES `db_computershop`.`articles` (`ArticleNo`),
  CONSTRAINT `FK_Rev_Cust`
    FOREIGN KEY (`CustomerNo`)
    REFERENCES `db_test`.`users` (`CustomerNo`))
ENGINE = InnoDB;

CREATE INDEX `FK_Rev_Art_idx` ON `db_test`.`reviews` (`ArticleNo` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
