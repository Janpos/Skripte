﻿-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: db_test
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `articlecategories`
--

LOCK TABLES `articlecategories` WRITE;
/*!40000 ALTER TABLE `articlecategories` DISABLE KEYS */;
INSERT INTO `articlecategories` VALUES (1,'PC','false'),(2,'Maus','true'),(3,'Monitor','false'),(4,'Notebook','false'),(5,'Tastatur','true');
/*!40000 ALTER TABLE `articlecategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Super PC 9000','Dieser PC ist super Level 9000.',1000.00,1,'Pictures/ArticlePicture_1.jpg',100,150,'active'),(2,'Speedy Gonzales','Die schnellste Maus von Mexiko.',40.00,2,'Pictures/ArticlePicture_2.jpg',100,150,'active'),(3,'Farbwucht 2.0 24\"','Monitor mit 24\" und den besten Farben aller Zeiten!',500.00,3,'Pictures/ArticlePicture_3.jpg',100,150,'active'),(4,'Logotech B100','Optical Mouse for Business, schwarz',5.99,2,'Pictures/Logotech_B100.png',100,150,'active'),(5,'Logotech LGT-M500','kabelgebundene Lasermaus',26.20,2,'Pictures/Logotech_LGT_M500.png',100,150,'active'),(6,'Logotech M185','schnurlose Maus',12.99,2,'Pictures/Logotech_M185.png',100,150,'active'),(7,'Pear Magic Mouse 2','Bluetooth, Multi-Touch, Space grau',112.83,2,'Pictures/Pear_Magic_Mouse_2.png',100,150,'active'),(8,'Pear Magic Mouse','Bluetooth, Multi-Touch, weiß',87.02,2,'Pictures/Pear_Magic_Mouse.png',100,150,'active'),(9,'Asas ROG Pugio','Gaming Maus, Rechts-/ Linkshänder, 7200 DPI, Aura Sync TGB Untersützung',71.99,2,'Pictures/Asas_ROG_Pugio.png',100,150,'active'),(10,'Pumacast LM20','Gaming Maus für FPS, RTS und MOBAs, 16.400 DPI, Laser-Sensor, LED, USB, 11 programmierbare Tasten',34.95,2,'Pictures/Pumacast_LM20.png',100,150,'active'),(11,'Pirixx MX-2000II','Programmierbare Gaming Lase Maus, 8 programmierbare Tasten, Avago 9500 Laser Sensor, 5600 DPI',34.99,2,'Pictures/Pirixx_MX_2000II.png',100,150,'active'),(12,'Asas UX300','Optische Maus, 5 Tasten, USB',12.90,2,'Pictures/Asas_UX300.jpg',100,150,'active'),(13,'Logotech G213','Gaming Tastatur mit RGB-Hintergrundbeleuchtung, QWERTZ Deutsches Tastaturlayout',54.99,5,'Pictures/Logotech_G213.png',100,150,'active'),(14,'Logotech K280e','Kabelgebunden, Business-Tastatur, QWERTZ Deutsches Tastaturlayout',22.69,5,'Pictures/Logotech_K280e.png',100,150,'active'),(15,'Logotech MK330','Kabellose Tastatur, USB, lange Akkulaufzeit, Kompatibel mit Windows und Chrome OS, QWERTZ Deutsches Tastaturlayout',31.99,5,'Pictures/Logotech_MK330.png',100,150,'active'),(16,'Pear Magic Tastatur','Tastatur mit Ziffernblock, Bluetooth, QWERTZ Deutsches Tastaturlayout',146.05,5,'Pictures/Pear_Magic_Tastatur.png',100,150,'active'),(17,'Pear MB100D/B','Kabelgebunden, QWERTZ Deutsches Tastaturlayout',138.00,5,'Pictures/Pear_MB100DB.png',100,150,'active'),(18,'Asas ROG GK2000','Gaming Tastatur mit RGB-Hintergrundbeleuchtung, Mechanisch, Cherry MX Red Switches, QWERTZ Deutsches Tastaturlayout',221.21,5,'Pictures/Asas_ROG_GK2000.png',100,150,'active'),(19,'Asas U2000','Kabelgebunden, QWERTZ Deutsches Tastaturlayout',27.97,5,'Pictures/Asas_U2000.jpg',100,150,'active'),(20,'Pumacast LK15','Gaming Tastatur, LED, USB, n-Key-Rollover, Millionen Farben, Makro Tasten',39.95,5,'Pictures/Pumacast_LK15.png',100,150,'active'),(21,'Pirixx512','Ergonomische Tastatur, geteiltes Tastenfeld, USB, Empfohlen bei Tennisarm, QWERTZ Deutsches Tastaturlayout',34.99,5,'Pictures/Pirixx512.png',100,150,'active'),(22,'Pupsiware X16','AMD 16 Kerne (16 x 3.4 GHz), wassergekühlt, 64 GB DDR4-3000',4599.99,1,'Pictures/Pupsiware_X16.png',100,150,'active'),(23,'Barbietornado 8.0','Gamer PC, 6 Kerne (6 x 3.7 GHz), 16 GB DDR4-2666 HyperX',1749.00,1,'Pictures/Barbietornado_8_0.png',100,150,'active'),(24,'Pupsiware Aurora R7','Intus Core i7-8700, 1 TB Optane, 6 Kerne (6 x 3.7 GHz)',1599.00,1,'Pictures/Pupsiware_Aurora_R7.png',100,150,'active'),(25,'Acker Aspire XC-780','Intus Core i3-7100, 4GB RAM, 1.000 GB HDD, Intus HD, Win10',529.00,1,'Pictures/Acker_Aspire_XC_780.png',100,150,'active'),(26,'Acker Aspire GX-781','Gaming PC, Intus Core i5-7400, 8 GB RAM, 1.000 GB HDD, Radeon RX480, Win10)',897.44,1,'Pictures/Acker_Aspire_GX_781.png',100,150,'active'),(27,'Dill Optilex 3050','3.4 GHz, 7500 Mini Tower',584.95,1,'Pictures/Dill_Optilex_3050.png',100,150,'active'),(28,'Dill PowerEdge0008','Intus Neon E3-1225, 1000 GB Festplatte, 8GB RAM, Win Server 2003',571.82,1,'Pictures/Dill_PowerEdge0008.png',100,150,'active'),(29,'HaPe Slimline','AHD Quad-Core A6-7310 APU, 8 GB RAM, 1 TB HDD',279.00,1,'Pictures/HaPe_Slimline.png',100,150,'active'),(30,'Intus GTX 1050Ti','Intus i5-7500 4 x 3.4 GHz, 32 GB DDR4',1059.00,1,'Pictures/Intus_GTX_1050Ti.png',100,150,'active'),(31,'Acker S242HLD','24 Zoll, VGA, DVI, HDMI, 1 ms Reaktionszeit',199.00,3,'Pictures/Acker_S242HLD.png',100,150,'active'),(32,'Sumsum S4F35','23,5 Zoll, schwarz',179.00,3,'Pictures/Sumsum_S4F35.jpg',100,150,'active'),(33,'Dill U21','27 Zoll, Eye-Care, HDMI, VGA, 2 ms Reaktionszeit, schwarz',219.00,3,'Pictures/Dill_U21.jpg',100,150,'active'),(34,'Asas VS24','25 Zoll, 8 ms Reaktionszeit, höhenverstellbar, schwarz',280.36,3,'Pictures/Asas_VS24.jpg',100,150,'active'),(35,'Sumsum PA242','24 Zoll, VGA, HDMI, 1 ms Reaktionszeit, schwarz',159.00,3,'Pictures/Sumsum_PA242.png',100,150,'active'),(36,'Viewson XG32','24,1 Zoll, 6 ms Reaktionszeit, HDMI',499.99,3,'Pictures/Viewson_XG32.jpg',100,150,'active'),(37,'Sumsum 34UM','32 Zoll, Curved Gaming Monitor, 144 HZ',659.99,3,'Pictures/Sumsum_34UM.jpg',100,150,'active'),(38,'Acker Predator','34 Zoll, HDMI, DisplayPort, Ultra Wide',499.95,3,'Pictures/Acker_Predator.png',100,150,'active'),(39,'BenKuh GL27','27 Zoll, Esports Monitor, 1 ms Reaktionszeit',499.00,3,'Pictures/BenKuh_GL27.png',100,150,'active'),(40,'Pear iBook','13,3 Zoll Display, Touchbar, Intus Quad-Core i5',1834.00,4,'Pictures/Pear_iBook.png',100,150,'active'),(41,'Denovo','17,3 Zoll, Intus Pentium 4415U Dual Core',366.00,4,'Pictures/Denovo.png',100,150,'active'),(42,'Acker Swift 1','13,3 Zoll, Full HD IPS matt, Ultrabook',479.00,4,'Pictures/Acker_Swift_1.png',100,150,'active'),(43,'Asos A4705UA','17,3 Zoll, FHD, Matt, Intus Core i5',579.00,4,'Pictures/Asos_A4705UA.png',100,150,'active'),(44,'HaPe Pavilion ','14 Zoll, Convertible, Intus Pentium 4415U',549.00,4,'Pictures/HaPe_Pavillion.png',100,150,'active'),(45,'Medium Erazer','15,6 Zoll, Intus Core i5-7200U',799.00,4,'Pictures/Medium_Erazer.png',100,150,'active'),(46,'Denoco','Notebook, 4415U Dual Core',366.00,4,'Pictures/Denoco.png',100,150,'active'),(47,'Viewson FAB','13 Zoll, Curved Gaming Monitor, 144 HZ',659.99,4,'Pictures/Viewson_FAB.png',100,150,'active'),(48,'Jota','Slim, 14 Zoll, Intus Celeron',329.99,4,'Pictures/Jota.png',100,150,'active'),(49,'Asos Notebook','17,3 ZOll, Intus N4200 Quad-Core',399.80,4,'Pictures/Asos_Notebook.png',100,150,'active');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `my_stopwords`
--

LOCK TABLES `my_stopwords` WRITE;
/*!40000 ALTER TABLE `my_stopwords` DISABLE KEYS */;
INSERT INTO `my_stopwords` VALUES ('ab'),('aber'),('alle'),('allem'),('allen'),('aller'),('allerdings'),('als'),('also'),('am'),('an'),('andere'),('anderem'),('anderen'),('anderer'),('andernfalls'),('anders'),('andersherum'),('anfangs'),('anhand'),('anschließend'),('ansonsten'),('anstatt'),('auch'),('auf'),('aufgrund'),('aus'),('außerdem'),('befindet'),('bei'),('beide'),('beim'),('beispielsweise'),('bereits'),('besonders'),('besteht'),('bestimmte'),('bestimmten'),('bestimmter'),('bevor'),('bietet'),('bis'),('bleiben'),('bringen'),('bringt'),('bsp'),('bzw'),('d.h'),('da'),('dabei'),('dafür'),('daher'),('damit'),('danach'),('dann'),('dar'),('daran'),('darauf'),('daraus'),('darf'),('darstellt'),('darüber'),('das'),('dass'),('davon'),('dazu'),('dem'),('demzufolge'),('den'),('denen'),('denn'),('der'),('deren'),('des'),('dessen'),('desto'),('die'),('dies'),('diese'),('diesem'),('diesen'),('dieser'),('dieses'),('doch'),('dort'),('durch'),('ebenfalls'),('eher'),('eigenen'),('eigentlich'),('ein'),('eine'),('einem'),('einen'),('einer'),('eines'),('einigen'),('einiges'),('einmal'),('einzelnen'),('entscheidend'),('entweder'),('er'),('erstmals'),('es'),('etc'),('etwas'),('euch'),('folgende'),('folgendem'),('folgenden'),('folgender'),('folgendes'),('folgt'),('für'),('ganz'),('gegen'),('gehen'),('gemacht'),('genannte'),('genannten'),('gerade'),('gerne'),('gibt'),('gilt'),('gleich'),('gleichen'),('gleichzeitig'),('habe'),('haben'),('hält'),('hat'),('hatte'),('hauptsächlich'),('her'),('heutigen'),('hier'),('hierbei'),('hierfür'),('hin'),('hingegen'),('hinzu'),('hoch'),('ihn'),('ihr'),('ihre'),('ihren'),('ihrer'),('im'),('immer'),('immerhin'),('in'),('indem'),('insgesamt'),('ist'),('ja'),('je'),('jede'),('jedem'),('jeder'),('jedes'),('jedoch'),('jetzt'),('jeweilige'),('jeweiligen'),('jeweils'),('kam'),('kann'),('keine'),('kommen'),('kommt'),('können'),('konnte'),('konnten'),('lassen'),('lässt'),('lautet'),('lediglich'),('leider'),('letztendlich'),('letztere'),('letzteres'),('liegt'),('machen'),('macht'),('mal'),('man'),('mehr'),('mehrere'),('meine'),('meinem'),('meisten'),('mich'),('mit'),('mithilfe'),('mittels'),('möchte'),('möglich'),('möglichst'),('momentan'),('muss'),('müssen'),('musste'),('nach'),('nachdem'),('nächsten'),('nahezu'),('nämlich'),('natürlich'),('neue'),('neuen'),('nicht'),('nichts'),('noch'),('nun'),('nur'),('ob'),('obwohl'),('oder'),('oftmals'),('ohne'),('per'),('sämtliche'),('scheint'),('schon'),('sehr'),('sein'),('seine'),('seinem'),('seinen'),('sich'),('sicherlich'),('sie'),('siehe'),('sind'),('so'),('sobald'),('sofern'),('solche'),('solchen'),('soll'),('sollen'),('sollte'),('sollten'),('somit'),('sondern'),('sorgt'),('sowie'),('sowohl'),('später'),('sprich'),('statt'),('trotz'),('über'),('überhaupt'),('um'),('und'),('uns'),('unter'),('usw'),('viel'),('viele'),('vielen'),('völlig'),('vom'),('von'),('vor'),('vorerst'),('vorher'),('während'),('war'),('wäre'),('waren'),('warum'),('was'),('weil'),('weitere'),('weiteren'),('weiterer'),('weiteres'),('weiterhin'),('welche'),('welchen'),('welcher'),('welches'),('wenn'),('wer'),('werden'),('wesentlich'),('wichtige'),('wichtigsten'),('wie'),('wieder'),('wiederum'),('will'),('wir'),('wird'),('wirklich'),('wo'),('wobei'),('worden'),('wurde'),('wurden'),('z.b'),('ziemlich'),('zu'),('zuerst'),('zum'),('zur'),('zusätzlich'),('zuvor'),('zwar'),('zwecks');
/*!40000 ALTER TABLE `my_stopwords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `orderpositions`
--

LOCK TABLES `orderpositions` WRITE;
/*!40000 ALTER TABLE `orderpositions` DISABLE KEYS */;
/*!40000 ALTER TABLE `orderpositions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `orderstatuses`
--

LOCK TABLES `orderstatuses` WRITE;
/*!40000 ALTER TABLE `orderstatuses` DISABLE KEYS */;
INSERT INTO `orderstatuses` VALUES (1,'Angelegt'),(2,'Bestätigt');
/*!40000 ALTER TABLE `orderstatuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `questionsandanswers`
--

LOCK TABLES `questionsandanswers` WRITE;
/*!40000 ALTER TABLE `questionsandanswers` DISABLE KEYS */;
INSERT INTO `questionsandanswers` VALUES (1,'John','Hallo, wo finde ich eure Telefonnumer?','Hi John, unsere Telefonnummer findest du entweder unter Impressum oder Beratung. Viele Grüße!','Telefonnummer','2018-08-17','true'),(2,'Peter','Hey, kann ich als Gast etwas bestellen?',NULL,'Gastbestellung','2018-08-16','false');
/*!40000 ALTER TABLE `questionsandanswers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (42,2,1,'Gar nicht so schnell die Maus.','Langsam','2018-12-09'),(43,2,2,'Nicht für CookieClicker geeignet. Taste springt ab.','Verarbeitung','2018-12-09'),(44,2,3,'Solide Maus','Ok','2018-12-09'),(45,2,4,'Die Maus ist on fire! Super geeignet für schnelle Aktionen bei SpeedClick','Super','2018-12-09'),(46,2,5,'Speedy Gonzales ist wirklich die schnellste Maus von Mexiko!','1 a','2018-12-09');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (42,'leon.stollberg@fhdw.de','404a7dbfcdf6298e65cea0eaf61c044f','Stollberg','Leon','','Herr','Meisenstraße','92','33607','Bielefeld','false','true'),(43,'oliver.neumann@fhdw.de','404a7dbfcdf6298e65cea0eaf61c044f','Neumann','Oliver','','Herr','Meisenstraße','92','33607','Bielefeld','false','true'),(44,'john.edwin.braun@fhdw.de','404a7dbfcdf6298e65cea0eaf61c044f','Braun','John Edwin','','Herr','Meisenstraße','92','33607','Bielefeld','false','true'),(45,'jan.niklas.posselt@fhdw.de','404a7dbfcdf6298e65cea0eaf61c044f','Posselt','Jan-Niklas','','Herr','Meisenstraße','92','33607','Bielefeld','false','true'),(46,'katharina.unruhe@fhdw.de','404a7dbfcdf6298e65cea0eaf61c044f','Unruhe','Katharina','','Frau','Meisenstraße','92','33607','Bielefeld','false','true');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-13 16:16:49
