-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema db_computershop
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `db_computershop` ;

-- -----------------------------------------------------
-- Schema db_computershop
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `db_computershop` ;
USE `db_computershop` ;

-- -----------------------------------------------------
-- Table `db_computershop`.`articlecategories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computershop`.`articlecategories` ;

CREATE TABLE IF NOT EXISTS `db_computershop`.`articlecategories` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Category` VARCHAR(45) NOT NULL,
  `Accessory` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
;


-- -----------------------------------------------------
-- Table `db_computershop`.`articles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computershop`.`articles` ;

CREATE TABLE IF NOT EXISTS `db_computershop`.`articles` (
  `ArticleNo` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Description` VARCHAR(999) NULL DEFAULT NULL,
  `Price` DECIMAL(12,2) NOT NULL,
  `Category` INT(11) NOT NULL,
  `PictureLink` VARCHAR(45) NOT NULL,
  `MinStock` INT(11) NOT NULL,
  `CurrentStock` INT(11) NOT NULL,
  `Status` VARCHAR(45) NOT NULL DEFAULT 'active',
  PRIMARY KEY (`ArticleNo`),
  CONSTRAINT `FK_Art_Cat`
    FOREIGN KEY (`Category`)
    REFERENCES `db_computershop`.`articlecategories` (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 1002
;

CREATE INDEX `FK_Art_Cat_idx` ON `db_computershop`.`articles` (`Category` ASC) VISIBLE;

CREATE FULLTEXT INDEX `Fulltext_Article` ON `db_computershop`.`articles` (`Name`, `Description`) VISIBLE;


-- -----------------------------------------------------
-- Table `db_computershop`.`my_stopwords`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computershop`.`my_stopwords` ;

CREATE TABLE IF NOT EXISTS `db_computershop`.`my_stopwords` (
  `value` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`value`))
ENGINE = InnoDB
;


-- -----------------------------------------------------
-- Table `db_computershop`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computershop`.`users` ;

CREATE TABLE IF NOT EXISTS `db_computershop`.`users` (
  `CustomerNo` INT(11) NOT NULL AUTO_INCREMENT,
  `EMail` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(45) NULL DEFAULT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `Firstname` VARCHAR(45) NOT NULL,
  `Title` VARCHAR(45) NULL DEFAULT NULL,
  `FormOfAddress` VARCHAR(45) NOT NULL,
  `InvoiceStreet` VARCHAR(45) NOT NULL,
  `InvoiceHouseNumber` VARCHAR(45) NOT NULL,
  `InvoicePostalCode` VARCHAR(45) NOT NULL,
  `InvoiceTown` VARCHAR(45) NOT NULL,
  `OneTimeCustomer` VARCHAR(45) NOT NULL,
  `Admin` VARCHAR(45) NOT NULL DEFAULT 'false',
  PRIMARY KEY (`CustomerNo`))
ENGINE = InnoDB
AUTO_INCREMENT = 39
;

CREATE UNIQUE INDEX `EMail_UNIQUE` ON `db_computershop`.`users` (`EMail` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `db_computershop`.`orderstatuses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computershop`.`orderstatuses` ;

CREATE TABLE IF NOT EXISTS `db_computershop`.`orderstatuses` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Statustext` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
;


-- -----------------------------------------------------
-- Table `db_computershop`.`orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computershop`.`orders` ;

CREATE TABLE IF NOT EXISTS `db_computershop`.`orders` (
  `OrderNo` INT(11) NOT NULL AUTO_INCREMENT,
  `CustomerNo` INT(11) NOT NULL,
  `Status` INT(11) NOT NULL,
  `DeliveryStreet` VARCHAR(45) NOT NULL,
  `DeliveryHouseNumber` VARCHAR(45) NOT NULL,
  `DeliveryPostalCode` VARCHAR(45) NOT NULL,
  `DeliveryTown` VARCHAR(45) NOT NULL,
  `InvoiceStreet` VARCHAR(45) NOT NULL,
  `InvoiceHouseNumber` VARCHAR(45) NOT NULL,
  `InvoicePostalCode` VARCHAR(45) NOT NULL,
  `InvoiceTown` VARCHAR(45) NOT NULL,
  `CreationDate` DATE NOT NULL,
  `PaymentMethod` VARCHAR(45) NOT NULL,
  `DispatchType` VARCHAR(45) NOT NULL,
  `DeliveryName` VARCHAR(45) NOT NULL,
  `DeliveryFirstname` VARCHAR(45) NOT NULL,
  `InvoiceName` VARCHAR(45) NOT NULL,
  `InvoiceFirstname` VARCHAR(45) NOT NULL,
  `DeliveryDate` DATE NOT NULL,
  PRIMARY KEY (`OrderNo`),
  CONSTRAINT `FK_Ord_Cust`
    FOREIGN KEY (`CustomerNo`)
    REFERENCES `db_computershop`.`users` (`CustomerNo`),
  CONSTRAINT `FK_Ord_Stat`
    FOREIGN KEY (`Status`)
    REFERENCES `db_computershop`.`orderstatuses` (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 16
;

CREATE INDEX `FK_Ord_Cust_idx` ON `db_computershop`.`orders` (`CustomerNo` ASC) VISIBLE;

CREATE INDEX `FK_Ord_Stat_idx` ON `db_computershop`.`orders` (`Status` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `db_computershop`.`orderpositions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computershop`.`orderpositions` ;

CREATE TABLE IF NOT EXISTS `db_computershop`.`orderpositions` (
  `OrderNo` INT(11) NOT NULL,
  `ArticleNo` INT(11) NOT NULL,
  `Amount` INT(11) NOT NULL,
  `PricePerPiece` DECIMAL(12,2) NOT NULL,
  PRIMARY KEY (`OrderNo`, `ArticleNo`),
  CONSTRAINT `FK_OrdPos_Art`
    FOREIGN KEY (`ArticleNo`)
    REFERENCES `db_computershop`.`articles` (`ArticleNo`),
  CONSTRAINT `FK_OrdPos_Ord`
    FOREIGN KEY (`OrderNo`)
    REFERENCES `db_computershop`.`orders` (`OrderNo`))
ENGINE = InnoDB
;

CREATE INDEX `FK_OrdPos_Art_idx` ON `db_computershop`.`orderpositions` (`ArticleNo` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `db_computershop`.`questionsandanswers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computershop`.`questionsandanswers` ;

CREATE TABLE IF NOT EXISTS `db_computershop`.`questionsandanswers` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Questioner` VARCHAR(45) NOT NULL,
  `Question` VARCHAR(400) NOT NULL,
  `Answer` VARCHAR(400) NULL DEFAULT NULL,
  `Title` VARCHAR(45) NOT NULL,
  `Date` DATE NOT NULL,
  `Active` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
;


-- -----------------------------------------------------
-- Table `db_computershop`.`reviews`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computershop`.`reviews` ;

CREATE TABLE IF NOT EXISTS `db_computershop`.`reviews` (
  `CustomerNo` INT(11) NOT NULL,
  `ArticleNo` INT(11) NOT NULL,
  `NumberOfStars` INT(11) NOT NULL,
  `Text` VARCHAR(1000) NULL DEFAULT NULL,
  `Title` VARCHAR(45) NOT NULL,
  `Date` DATE NOT NULL,
  PRIMARY KEY (`CustomerNo`, `ArticleNo`),
  CONSTRAINT `FK_Rev_Art`
    FOREIGN KEY (`ArticleNo`)
    REFERENCES `db_computershop`.`articles` (`ArticleNo`),
  CONSTRAINT `FK_Rev_Cust`
    FOREIGN KEY (`CustomerNo`)
    REFERENCES `db_computershop`.`users` (`CustomerNo`))
ENGINE = InnoDB;

CREATE INDEX `FK_Rev_Art_idx` ON `db_computershop`.`reviews` (`ArticleNo` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
