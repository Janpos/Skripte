-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: db_computershop
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `articlecategories`
--

LOCK TABLES `articlecategories` WRITE;
/*!40000 ALTER TABLE `articlecategories` DISABLE KEYS */;
INSERT INTO `articlecategories` VALUES (1,'PC','false'),(2,'Maus','true'),(3,'Monitor','false'),(4,'Notebook','false'),(5,'Tastatur','true');
/*!40000 ALTER TABLE `articlecategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Super PC 9000','Und Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,<br> - sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.<br> - Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem<br> - ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor<br> <br>invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero <br> - eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea <br> - takimata sanctus auch est Lorem ipsum dolor sit amet. Lorem ip<br>',2001.00,1,'Pictures/ArticlePicture_1.jpg',100,150,'active'),(2,'Speedy Gonzales','Die schnellste Maus von Mexiko.',40.00,2,'Pictures/ArticlePicture_2.jpg',200,190,'active'),(3,'Farbwucht 2.0 24\"','Monitor mit 24\" und den besten Farben aller Zeiten!',500.00,3,'Pictures/ArticlePicture_3.jpg',50,60,'active'),(4,'Logotech B100','Optical Mouse for Business, schwarz',6.00,2,'Pictures/Logotech_B100.png',100,150,'active'),(5,'Logotech LGT-M500','kabelgebundene Lasermaus',26.00,2,'Pictures/Logotech_B100.png',100,150,'active'),(6,'Logotech M185','schnurlose Maus',13.00,2,'Pictures/Logotech_B100.png',100,150,'active'),(7,'Pear Magic Mouse 2','Bluetooth, Multi-Touch, Space grau',113.00,2,'Pictures/Logotech_B100.png',100,150,'active'),(8,'Pear Magic Mouse','Bluetooth, Multi-Touch, weiß',87.00,2,'Pictures/Logotech_B100.png',100,150,'active'),(9,'Asas ROG Pugio','Gaming Maus, Rechts-/ Linkshänder, 7200 DPI, Aura Sync TGB Untersützung',72.00,2,'Pictures/Asas_ROG_Pugio.png',100,150,'active'),(10,'Pumacast LM20','Gaming Maus für FPS, RTS und MOBAs, 16.400 DPI, Laser-Sensor, LED, USB, 11 programmierbare Tasten',35.00,2,'Pictures/Pumacast_LM20.png',100,150,'active'),(11,'Pirixx MX-2000II','Programmierbare Gaming Lase Maus, 8 programmierbare Tasten, Avago 9500 Laser Sensor, 5600 DPI',35.00,2,'Pictures/ArticlePicture_2.jpg',100,150,'active'),(12,'Asas UX300','Optische Maus, 5 Tasten, USB',13.00,2,'Pictures/Asas_ROG_Pugio.png',100,150,'active'),(13,'Logotech G213','Gaming Tastatur mit RGB-Hintergrundbeleuchtung, QWERTZ Deutsches Tastaturlayout',55.00,5,'Pictures/Logotech_G213.png',100,150,'active'),(14,'Logotech K280e','Kabelgebunden, Business-Tastatur, QWERTZ Deutsches Tastaturlayout',23.00,5,'Pictures/Logotech_K280e.png',100,150,'active'),(15,'Logotech MK330','Kabellose Tastatur, USB, lange Akkulaufzeit, Kompatibel mit Windows und Chrome OS, QWERTZ Deutsches Tastaturlayout',32.00,5,'Pictures/Logotech_MK330.png',100,150,'active'),(16,'Pear Magic Tastatur','Tastatur mit Ziffernblock, Bluetooth, QWERTZ Deutsches Tastaturlayout',146.00,5,'Pictures/Logotech_G213.png',100,150,'active'),(17,'Pear MB100D/B','Kabelgebunden, QWERTZ Deutsches Tastaturlayout',138.00,5,'Pictures/Logotech_K280e.png',100,150,'active'),(18,'Asas ROG GK2000','Gaming Tastatur mit RGB-Hintergrundbeleuchtung, Mechanisch, Cherry MX Red Switches, QWERTZ Deutsches Tastaturlayout',221.00,5,'Pictures/Logotech_MK330.png',100,150,'active'),(19,'Asas U2000','Kabelgebunden, QWERTZ Deutsches Tastaturlayout',28.00,5,'Pictures/Pumacast_LK15.png',100,150,'active'),(20,'Pumacast LK15','Gaming Tastatur, LED, USB, n-Key-Rollover, Millionen Farben, Makro Tasten',40.00,5,'Pictures/Pumacast_LK15.png',100,150,'active'),(21,'Pirixx512','Ergonomische Tastatur, geteiltes Tastenfeld, USB, Empfohlen bei Tennisarm, QWERTZ Deutsches Tastaturlayout',35.00,5,'Pictures/Pumacast_LK15.png',100,150,'active'),(22,'Pupsiware X16','AMD 16 Kerne (16 x 3.4 GHz), wassergekühlt, 64 GB DDR4-3000',4599.99,1,'Pictures/Pupsiware_X16.png',100,150,'active'),(23,'Barbietornado 8.0','Gamer PC, 6 Kerne (6 x 3.7 GHz), 16 GB DDR4-2666 HyperX',1749.00,1,'Pictures/Barbietornado_8_0.png',100,150,'active'),(24,'Pupsiware Aurora R7','Intus Core i7-8700, 1 TB Optane, 6 Kerne (6 x 3.7 GHz)',1599.00,1,'Pictures/Pupsiware_Aurora_R7.png',100,150,'active'),(25,'Acker Aspire XC-780','Intus Core i3-7100, 4GB RAM, 1.000 GB HDD, Intus HD, Win10',529.00,1,'Pictures/Acker_Aspire_XC_780.png',100,150,'active'),(26,'Acker Aspire GX-781sdfcssdsddfvb','Gaming PC, Intus Core i5-7400, 8 GB RAM, 1.000 GB HDD, Radeon RX480, Win10)',879.00,1,'Pictures/Acker_Aspire_XC_780.png',100,150,'active'),(27,'Dill Optilex 3050','3.4 GHz, 7500 Mini Tower',585.00,1,'Pictures/Dill_Optilex_3050.png',100,150,'active'),(28,'Dill PowerEdge0008','Intus Neon E3-1225, 1000 GB Festplatte, 8GB RAM, Win Server 2003',572.00,1,'Pictures/Dill_PowerEdge0008.png',100,150,'active'),(29,'HaPe Slimline','AHD Quad-Core A6-7310 APU, 8 GB RAM, 1 TB HDD',279.00,1,'Pictures/ArticlePicture_1.jpg',100,150,'active'),(30,'Intus GTX 1050Ti','Intus i5-7500 4 x 3.4 GHz, 32 GB DDR4',1059.00,1,'Pictures/Intus_GTX_1050Ti.png',100,150,'active'),(31,'Acker S242HLD','24 Zoll, VGA, DVI, HDMI, 1 ms Reaktionszeit',199.00,3,'Pictures/Intus_GTX_1050Ti.png',100,150,'active'),(32,'Sumsum S4F35','23,5 Zoll, schwarz',179.00,3,'Pictures/images.jpg',100,150,'active'),(33,'Dill U21','27 Zoll, Eye-Care, HDMI, VGA, 2 ms Reaktionszeit, schwarz',219.00,3,'Pictures/DELL_U2715H_01.png',100,150,'active'),(34,'Asas VS24','25 Zoll, 8 ms Reaktionszeit, höhenverstellbar, schwarz',280.00,3,'Pictures/monitor-22.png',100,150,'active'),(35,'Sumsum PA242','24 Zoll, VGA, HDMI, 1 ms Reaktionszeit, schwarz',159.00,3,'Pictures/images.jpg',100,150,'active'),(36,'Viewson XG32','24,1 Zoll, 6 ms Reaktionszeit, HDMI',500.00,3,'Pictures/Dill_PowerEdge0008.png',100,150,'active'),(37,'Sumsum 34UM','32 Zoll, Curved Gaming Monitor, 144 HZ',660.00,3,'Pictures/monitor-22.png',100,150,'active'),(38,'Acker Predator','34 Zoll, HDMI, DisplayPort, Ultra Wide',500.00,3,'Pictures/Dill_PowerEdge0008.png',100,150,'active'),(39,'BenKuh GL27','27 Zoll, Esports Monitor, 1 ms Reaktionszeit',500.00,3,'Pictures/Dill_PowerEdge0008.png',100,150,'active'),(40,'fghj','wdrtfgzuhnjiko',567.00,1,'/Pictures',1,1,'active'),(41,'fghj','wdrtfgzuhnjiko',567.00,1,'/Pictures',1,1,'active'),(42,'fghj','wdrtfgzuhnjiko',567.00,1,'/Pictures',1,1,'active'),(43,'iwjd','iwjsdnc',1.00,1,'sikdjnc',1,1,'active'),(44,'iwjd','iwjsdnc',1.00,1,'sikdjnc',1,1,'active'),(999,'iwjd','iwjsdnc',1.00,1,'sikdjnc',1,1,'active'),(1000,'iwjd','iwjsdnc',1.00,1,'sikdjnc',1,1,'active'),(1001,'fghj','wdrtfgzuhnjiko',567.00,1,'/Pictures',1,1,'active');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `my_stopwords`
--

LOCK TABLES `my_stopwords` WRITE;
/*!40000 ALTER TABLE `my_stopwords` DISABLE KEYS */;
INSERT INTO `my_stopwords` VALUES ('ab'),('aber'),('alle'),('allem'),('allen'),('aller'),('allerdings'),('als'),('also'),('am'),('an'),('andere'),('anderem'),('anderen'),('anderer'),('andernfalls'),('anders'),('andersherum'),('anfangs'),('anhand'),('anschließend'),('ansonsten'),('anstatt'),('auch'),('auf'),('aufgrund'),('aus'),('außerdem'),('befindet'),('bei'),('beide'),('beim'),('beispielsweise'),('bereits'),('besonders'),('besteht'),('bestimmte'),('bestimmten'),('bestimmter'),('bevor'),('bietet'),('bis'),('bleiben'),('bringen'),('bringt'),('bsp'),('bzw'),('d.h'),('da'),('dabei'),('dafür'),('daher'),('damit'),('danach'),('dann'),('dar'),('daran'),('darauf'),('daraus'),('darf'),('darstellt'),('darüber'),('das'),('dass'),('davon'),('dazu'),('dem'),('demzufolge'),('den'),('denen'),('denn'),('der'),('deren'),('des'),('dessen'),('desto'),('die'),('dies'),('diese'),('diesem'),('diesen'),('dieser'),('dieses'),('doch'),('dort'),('durch'),('ebenfalls'),('eher'),('eigenen'),('eigentlich'),('ein'),('eine'),('einem'),('einen'),('einer'),('eines'),('einigen'),('einiges'),('einmal'),('einzelnen'),('entscheidend'),('entweder'),('er'),('erstmals'),('es'),('etc'),('etwas'),('euch'),('folgende'),('folgendem'),('folgenden'),('folgender'),('folgendes'),('folgt'),('für'),('ganz'),('gegen'),('gehen'),('gemacht'),('genannte'),('genannten'),('gerade'),('gerne'),('gibt'),('gilt'),('gleich'),('gleichen'),('gleichzeitig'),('habe'),('haben'),('hält'),('hat'),('hatte'),('hauptsächlich'),('her'),('heutigen'),('hier'),('hierbei'),('hierfür'),('hin'),('hingegen'),('hinzu'),('hoch'),('ihn'),('ihr'),('ihre'),('ihren'),('ihrer'),('im'),('immer'),('immerhin'),('in'),('indem'),('insgesamt'),('ist'),('ja'),('je'),('jede'),('jedem'),('jeder'),('jedes'),('jedoch'),('jetzt'),('jeweilige'),('jeweiligen'),('jeweils'),('kam'),('kann'),('keine'),('kommen'),('kommt'),('können'),('konnte'),('konnten'),('lassen'),('lässt'),('lautet'),('lediglich'),('leider'),('letztendlich'),('letztere'),('letzteres'),('liegt'),('machen'),('macht'),('mal'),('man'),('mehr'),('mehrere'),('meine'),('meinem'),('meisten'),('mich'),('mit'),('mithilfe'),('mittels'),('möchte'),('möglich'),('möglichst'),('momentan'),('muss'),('müssen'),('musste'),('nach'),('nachdem'),('nächsten'),('nahezu'),('nämlich'),('natürlich'),('neue'),('neuen'),('nicht'),('nichts'),('noch'),('nun'),('nur'),('ob'),('obwohl'),('oder'),('oftmals'),('ohne'),('per'),('sämtliche'),('scheint'),('schon'),('sehr'),('sein'),('seine'),('seinem'),('seinen'),('sich'),('sicherlich'),('sie'),('siehe'),('sind'),('so'),('sobald'),('sofern'),('solche'),('solchen'),('soll'),('sollen'),('sollte'),('sollten'),('somit'),('sondern'),('sorgt'),('sowie'),('sowohl'),('später'),('sprich'),('statt'),('trotz'),('über'),('überhaupt'),('um'),('und'),('uns'),('unter'),('usw'),('viel'),('viele'),('vielen'),('völlig'),('vom'),('von'),('vor'),('vorerst'),('vorher'),('während'),('war'),('wäre'),('waren'),('warum'),('was'),('weil'),('weitere'),('weiteren'),('weiterer'),('weiteres'),('weiterhin'),('welche'),('welchen'),('welcher'),('welches'),('wenn'),('wer'),('werden'),('wesentlich'),('wichtige'),('wichtigsten'),('wie'),('wieder'),('wiederum'),('will'),('wir'),('wird'),('wirklich'),('wo'),('wobei'),('worden'),('wurde'),('wurden'),('z.b'),('ziemlich'),('zu'),('zuerst'),('zum'),('zur'),('zusätzlich'),('zuvor'),('zwar'),('zwecks');
/*!40000 ALTER TABLE `my_stopwords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `orderpositions`
--

LOCK TABLES `orderpositions` WRITE;
/*!40000 ALTER TABLE `orderpositions` DISABLE KEYS */;
INSERT INTO `orderpositions` VALUES (1,1,1,0.00),(1,2,2,0.00),(2,1,2,0.00),(3,3,1,0.00),(7,1,2,99.00),(8,1,2,99.99),(15,9,1,72.00),(15,41,1,567.00);
/*!40000 ALTER TABLE `orderpositions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,1,1,'ijij','iji','ji','ji','ji','jijoij','oijoi','oiji','0000-00-00','','','','','','','0000-00-00'),(2,2,2,'ihgg','ftf','huhu','tftf','ftftf','tftf','tftf','tf','0000-00-00','','','','','','','0000-00-00'),(3,3,1,'pppp','ppppp','plplpl','plplpl','lplpl','plplpl','plplpl','llplp','0000-00-00','','','','','','','0000-00-00'),(5,37,1,'Vor den Driften','5','32423','Minden','Vor den Driften','null','32423','Minden','2018-09-06','PayPal','null','john2','John','','','0000-00-00'),(6,37,1,'Vor den Driften','5','32423','Minden','Vor den Driften','null','32423','Minden','2018-09-06','Rechnung','null','Braun','John','','','0000-00-00'),(7,37,1,'Vor den Driften','5','32423','Minden','Vor den Driften','null','32423','Minden','2018-09-06','Rechnung','null','Braun','John','','','0000-00-00'),(8,37,1,'Vor den Driften','5','32423','Minden','Vor den Driften','null','32423','Minden','2018-09-06','Rechnung','null','Braun','John','','','0000-00-00'),(9,37,1,'Vor den Driften','5','32423','Minden','Vor den Driften','null','32423','Minden','2018-09-06','Rechnung','Premium-Versand (1-3 Tage) +3 EUR','Braun','John','','','0000-00-00'),(10,37,1,'Vor den Driften','5','32423','Minden','Vor den Driften','5','32423','Minden','2018-09-06','Rechnung','Standardversand (3-5 Tage)','Braun','John','Braun','John','0000-00-00'),(11,37,1,'Vor den Driften','5','32423','Minden','Vor den Driften','5','32423','Minden','2018-09-06','Rechnung','Standardversand (3-5 Tage)','Braun','John','Braun','John','0000-00-00'),(12,37,1,'Vor den Driften','5','32423','Minden','Vor den Driften','5','32423','Minden','2018-09-06','Rechnung','Standardversand (3-5 Tage)','Braun','John','Braun','John','0000-00-00'),(13,37,1,'Vor den Driften','5','32423','Minden','Vor den Driften','5','32423','Minden','2018-09-06','Rechnung','Standardversand (3-5 Tage)','Braun','John','Braun','John','0000-00-00'),(14,37,1,'Vor den Driften','5','32423','Minden','Vor den Driften','5','32423','Minden','2018-09-06','Rechnung','Standardversand (3-5 Tage)','Braun','John','Braun','John','0000-00-00'),(15,37,1,'Vor den Driften','5','32423','Minden','Vor den Driften','5','32423','Minden','2018-09-07','Rechnung','Standardversand (3-5 Tage)','Braun','John','Braun','John','0000-00-00');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `orderstatuses`
--

LOCK TABLES `orderstatuses` WRITE;
/*!40000 ALTER TABLE `orderstatuses` DISABLE KEYS */;
INSERT INTO `orderstatuses` VALUES (1,'Angelegt'),(2,'Bestätigt');
/*!40000 ALTER TABLE `orderstatuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `questionsandanswers`
--

LOCK TABLES `questionsandanswers` WRITE;
/*!40000 ALTER TABLE `questionsandanswers` DISABLE KEYS */;
INSERT INTO `questionsandanswers` VALUES (1,'John','Hallo, wo finde ich eure Telefonnumer?','Hi John, unsere Telefonnummer findest du entweder unter Impressum oder Beratung. Viele Grüße!','Telefonnummer','2018-08-17','true'),(2,'Peter','Hey, kann ich als Gast etwas bestellen?',NULL,'Gastbestellung','2018-08-16','false');
/*!40000 ALTER TABLE `questionsandanswers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (3,3,1,'Kaputt!','Nach 1 Woche kaputt!1','2018-08-31'),(7,1,1,'','test65','2018-08-31'),(7,2,1,'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.     Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu','Nach 1 Woche kaputt!2','2018-08-31'),(7,5,1,'okoko','kokok','2018-08-31'),(7,15,1,'<h1>TEST</h1>','TEst','2018-08-31'),(7,20,1,'rfgvhbbjnk','ighjk','2018-08-31'),(7,26,1,'','tes5','2018-08-31'),(7,33,4,'idfuhuqwefhuqdh','jhdjdh','2018-08-31'),(8,1,1,'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.     Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu','Nach 1 Woche kaputt!2','2018-08-31'),(13,1,1,'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.     Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu','Nach 1 Woche kaputt!2','2018-08-31'),(14,1,1,'Kaputt!','Nach 1 Woche kaputt!3','2018-08-31'),(15,1,1,'Kaputt!','Nach 1 Woche kaputt!4','2018-08-31'),(16,1,1,'Kaputt!','Nach 1 Woche kaputt!5','2018-08-31'),(17,1,1,'Kaputt!','Nach 1 Woche kaputt!6','2018-08-31');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (7,'7','null','Anonym','0','0','Anderes','0','0','0','0','false','false'),(8,'test@test.de','asAS12!\"qwert','test','test','','Herr','null','null','null','null','false','false'),(10,'test1','1234','Braun','John','','Herr','null','null','null','null','false','false'),(13,'delete','1234','Braun','John','','Herr','null','null','null','null','false','true'),(14,'3','1234','Braun','John','','Herr','null','null','null','null','false','false'),(15,'4','1234','Braun','John','','Herr','null','null','null','null','false','false'),(16,'5','1234','Braun','John','','Herr','null','null','null','null','false','false'),(17,'6','1234','Braun','John','','Herr','null','null','null','null','false','false'),(20,'null','null','Anonym','0','0','Anderes','0','0','0','0','false','false'),(22,'klm@idfdhjisdf.de','qqQQ11!!12345','kk','lm','djk','Herr','null','null','null','null','false','false'),(23,'opk@kol.de','qqQQ11!!qwert','k','opk','k','Herr','null','null','null','null','false','false'),(24,'lp@kodk.de',NULL,'plk','plp','Ã¼p','Herr','pl','pl','pl','pl','true','false'),(25,'oadfnofhn@ksjfifjs.de',NULL,'plk','plp','Ã¼p','Herr','pl','pl','pl','pl','true','false'),(26,'26','null','Anonym','0','0','Anderes','0','0','0','0','false','false'),(27,'iwjendf','','','','','','','','','','','false'),(28,'woekdfcmvn@gmail.com',NULL,'Braun','1445','','Herr','','','','','true','false'),(30,'wsidjn@owsdkm.de',NULL,'Braun','1951','','Herr','Vor den Driften','5','32423','Minden','true','false'),(33,'',NULL,'','','','Herr','','','','','true','false'),(34,'1952@d.d',NULL,'Braun','John','','Herr','Vor den Driften','5','32423','Minden','true','false'),(35,'22@35.de',NULL,'Braun','To','','Herr','Vor den Driften','5','32423','Minden','true','false'),(36,'07@23.de',NULL,'Braun','John','','Herr','Vor den Driften','5','32423','Minden','true','false'),(37,'john.edwin.braun@gmail.com','1bfade1e6c979b025c80a0ecfa917606','Braun','John','','Herr','Vor den Driften','5','32423','Minden','false','false'),(38,'10@53.de',NULL,'Braun','John','','Herr','Vor den Driften','5','32423','Minden','true','false');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-08 17:23:54
