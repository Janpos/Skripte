-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema db_computer_shop
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `db_computer_shop` ;

-- -----------------------------------------------------
-- Schema db_computer_shop
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `db_computer_shop` ;
USE `db_computer_shop` ;

-- -----------------------------------------------------
-- Table `db_computer_shop`.`articlecategories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computer_shop`.`articlecategories` ;

CREATE TABLE IF NOT EXISTS `db_computer_shop`.`articlecategories` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Category` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 8;


-- -----------------------------------------------------
-- Table `db_computer_shop`.`articles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computer_shop`.`articles` ;

CREATE TABLE IF NOT EXISTS `db_computer_shop`.`articles` (
  `ArticleNo` INT(11) NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Description` VARCHAR(200) NULL DEFAULT NULL,
  `Price` INT(11) NOT NULL,
  `Category` INT(11) NOT NULL,
  `PictureLink` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ArticleNo`),
  INDEX `FK_Art_Cat_idx` (`Category` ASC) VISIBLE,
  CONSTRAINT `FK_Art_Cat`
    FOREIGN KEY (`Category`)
    REFERENCES `db_computer_shop`.`articlecategories` (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
;


-- -----------------------------------------------------
-- Table `db_computer_shop`.`customers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computer_shop`.`customers` ;

CREATE TABLE IF NOT EXISTS `db_computer_shop`.`customers` (
  `CustomerNo` INT(11) NOT NULL AUTO_INCREMENT,
  `E-Mail` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(45) NULL DEFAULT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `Firstname` VARCHAR(45) NOT NULL,
  `Title` VARCHAR(45) NULL DEFAULT NULL,
  `FormOfAddress` VARCHAR(45) NOT NULL,
  `DeliveryStreet` VARCHAR(45) NOT NULL,
  `DeliveryHouseNumber` VARCHAR(45) NOT NULL,
  `DeliveryPostalCode` VARCHAR(45) NOT NULL,
  `DeliveryTown` VARCHAR(45) NOT NULL,
  `InvoiceStreet` VARCHAR(45) NOT NULL,
  `InvoiceHouseNumber` VARCHAR(45) NOT NULL,
  `InvoicePostalCode` VARCHAR(45) NOT NULL,
  `InvoiceTown` VARCHAR(45) NOT NULL,
  `OneTimeCustomer` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`CustomerNo`))
ENGINE = InnoDB
AUTO_INCREMENT = 4;


-- -----------------------------------------------------
-- Table `db_computer_shop`.`orderstatuses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computer_shop`.`orderstatuses` ;

CREATE TABLE IF NOT EXISTS `db_computer_shop`.`orderstatuses` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Statustext` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
;


-- -----------------------------------------------------
-- Table `db_computer_shop`.`orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computer_shop`.`orders` ;

CREATE TABLE IF NOT EXISTS `db_computer_shop`.`orders` (
  `OrderNo` INT(11) NOT NULL AUTO_INCREMENT,
  `CustomerNo` INT(11) NOT NULL,
  `Status` INT(11) NOT NULL,
  PRIMARY KEY (`OrderNo`),
  INDEX `FK_Ord_Cust_idx` (`CustomerNo` ASC) VISIBLE,
  INDEX `FK_Ord_Stat_idx` (`Status` ASC) VISIBLE,
  CONSTRAINT `FK_Ord_Cust`
    FOREIGN KEY (`CustomerNo`)
    REFERENCES `db_computer_shop`.`customers` (`CustomerNo`),
  CONSTRAINT `FK_Ord_Stat`
    FOREIGN KEY (`Status`)
    REFERENCES `db_computer_shop`.`orderstatuses` (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
;


-- -----------------------------------------------------
-- Table `db_computer_shop`.`orderpositions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computer_shop`.`orderpositions` ;

CREATE TABLE IF NOT EXISTS `db_computer_shop`.`orderpositions` (
  `OrderNo` INT(11) NOT NULL,
  `ArticleNo` INT(11) NOT NULL,
  `Amount` INT(11) NOT NULL,
  PRIMARY KEY (`OrderNo`, `ArticleNo`),
  INDEX `FK_OrdPos_Art_idx` (`ArticleNo` ASC) VISIBLE,
  CONSTRAINT `FK_OrdPos_Art`
    FOREIGN KEY (`ArticleNo`)
    REFERENCES `db_computer_shop`.`articles` (`ArticleNo`),
  CONSTRAINT `FK_OrdPos_Ord`
    FOREIGN KEY (`OrderNo`)
    REFERENCES `db_computer_shop`.`orders` (`OrderNo`))
ENGINE = InnoDB
;


-- -----------------------------------------------------
-- Table `db_computer_shop`.`questionsandanswers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computer_shop`.`questionsandanswers` ;

CREATE TABLE IF NOT EXISTS `db_computer_shop`.`questionsandanswers` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Questioner` VARCHAR(45) NOT NULL,
  `Question` VARCHAR(400) NOT NULL,
  `Answer` VARCHAR(400) NULL DEFAULT NULL,
  `Title` VARCHAR(45) NOT NULL,
  `Date` DATE NOT NULL,
  `Active` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
;


-- -----------------------------------------------------
-- Table `db_computer_shop`.`reviews`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `db_computer_shop`.`reviews` ;

CREATE TABLE IF NOT EXISTS `db_computer_shop`.`reviews` (
  `CustomerNo` INT(11) NOT NULL,
  `ArticleNo` INT(11) NOT NULL,
  `NumberOfStars` INT(11) NOT NULL,
  `Text` VARCHAR(400) NULL DEFAULT NULL,
  `Title` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`CustomerNo`, `ArticleNo`),
  INDEX `FK_Rev_Art_idx` (`ArticleNo` ASC) VISIBLE,
  CONSTRAINT `FK_Rev_Art`
    FOREIGN KEY (`ArticleNo`)
    REFERENCES `db_computer_shop`.`articles` (`ArticleNo`),
  CONSTRAINT `FK_Rev_Cust`
    FOREIGN KEY (`CustomerNo`)
    REFERENCES `db_computer_shop`.`customers` (`CustomerNo`))
ENGINE = InnoDB
;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
